# How to name partials

Partials in this directory follow the **BEM (Block, Element, Modifier)** syntax. In CSS, the syntax is as follows:

```css
    .block {}
    .block__element {}
    .block--modifier
```

### Get up to speed by reading:

  * [Mind BEMing by Harry Roberts](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)
  * [BEM.info (the BEM docs site)](http://bem.info/method/definitions/)

### WVU Masthead & Footer partials

In our case, we're likely to have many different versions of a masthead or footer. Name them like:

    _masthead--v1.html
    _masthead--v2.html
    _masthead--v3.html

Note the double dash (--) for each version number. The double dash denotes that it's a modifier.

For footer elements:

    _footer__contact--v1.html
    
In this case, `_footer` is the block, `__contact` is the element, and `--v1` is the modifier. We're likely to have many elements and versions, like so:

    _footer__contact--v1.html
    _footer__contact--v2.html
    
    _footer__credits--v1.html
    _footer__credits--v2.html
    
    _footer__icons--v1.html
    _footer__icons--v2.html
    
If you have questions about this syntax, contact [Adam Johnson](mailto:adam.johnson@mail.wvu.edu).