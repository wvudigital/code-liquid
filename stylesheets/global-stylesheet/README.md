# Compiling global stylesheets:

Versions 3.0 and 4.0 of the global stylesheet have been compiled with [Codekit v1](http://incident57.com/codekit/). Drag and drop the parent folder ("3.0", "4.0", etc) onto CodeKit to get up and running quickly.

Version 5.0 has been compiled with [Prepros](https://prepros.io/) v5.2.0 (free version). Again, drag and drop the parent folder onto Prepros to get up and running quickly.

Version 6.0 has been compiled with Prepros v5.8.0.